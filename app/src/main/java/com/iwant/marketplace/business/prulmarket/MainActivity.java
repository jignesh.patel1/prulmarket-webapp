package com.iwant.marketplace.business.prulmarket;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.support.customtabs.CustomTabsCallback;
import android.support.customtabs.CustomTabsClient;
import android.support.customtabs.CustomTabsIntent;
import android.support.customtabs.CustomTabsServiceConnection;
import android.support.customtabs.CustomTabsSession;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.CookieManager;
import android.webkit.JsResult;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.Toast;

import org.chromium.customtabsclient.shared.CustomTabsHelper;
import org.chromium.customtabsclient.shared.ServiceConnection;
import org.chromium.customtabsclient.shared.ServiceConnectionCallback;

@SuppressLint("SetJavaScriptEnabled")
public class MainActivity extends Activity implements ServiceConnectionCallback{

    public static String CHROME_PACKAGE_NAME = "com.android.chrome";
    public static final String EXTRA_URL = "extra.url";
    private final int CHROME_CUSTOM_TAB_REQUEST_CODE = 1100;
    AlertDialog alertDialog;

    public static final String USER_AGENT = "Mozilla/5.0 (Linux; Android 5.1.1; Nexus 5 Build/LMY48B; wv) AppleWebKit/537.36 (KHTML, like Gecko) " +
            "Version/4.0 Chrome/74.0.3729.112 Mobile Safari/537.36";
    public static final int REQUEST_SELECT_FILE = 100;
    public ValueCallback<Uri[]> uploadMessage;
    WebView mainWebView;

    private String target_url_prefix = "prulmarket.com";
    private WebView mWebviewPop;
    private FrameLayout mContainer;


    private CustomTabsSession mCustomTabsSession;
    private CustomTabsClient mClient;
    private CustomTabsServiceConnection mConnection;
    private String mPackageNameToBind;
    private String notificationUrl;

   // CordovaWebView cordovaWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bindCustomTabsService();
        alertDialog = new AlertDialog.Builder(this).create();

        // final View controlsView =
        // findViewById(R.id.fullscreen_content_controls);
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.setAcceptCookie(true);

        mainWebView = (WebView) findViewById(R.id.webview);
        mContainer = (FrameLayout) findViewById(R.id.webview_frame);

        WebSettings webSettings = mainWebView.getSettings();
        webSettings.setDefaultTextEncodingName("utf-8");

        //webSettings.setUserAgentString("Mozilla/5.0 (Linux; Android 4.1.2; C1905 Build/15.1.C.2.8) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.112 Mobile Safari/537.36");
        mainWebView.getSettings().setUserAgentString(getString(R.string.app_name));
        mainWebView.setWebContentsDebuggingEnabled(true);
        webSettings.setDomStorageEnabled(true);// Add this
        webSettings.setDatabaseEnabled(true);// Add this
        webSettings.setJavaScriptEnabled(true);

        webSettings.setAppCacheEnabled(true);
        webSettings.setSupportMultipleWindows(true);
        //  webSettings.setSupportZoom(true);
        //  webSettings.setBuiltInZoomControls(true);
        //  webSettings.setDisplayZoomControls(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setPluginState(WebSettings.PluginState.ON);

        webSettings.setAllowFileAccess(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);

//        webSettings.setUserAgentString(getResources().getString(R.string.app_name   ));


        mainWebView.setWebViewClient(new myWebClient());
        mainWebView.setWebChromeClient(new MyWebChromeClient());
        //mainWebView.loadUrl("https://prulmarket.com/2PfkYA2Mg9/home");


        if(this.getIntent().hasExtra("URL") && null != this.getIntent().getStringExtra("URL")){
            notificationUrl = this.getIntent().getStringExtra("URL");
        }

        if(null == notificationUrl) {
            mainWebView.loadUrl("https://prulmarket.com/2PfkYA2Mg9/home");
            //mainWebView.loadUrl("http://richeyrichinfotech.iwantunlimited.com:794/2PfkYA2Mg9/home");
        }else{
            mainWebView.loadUrl(notificationUrl);
        }



    }

    // To handle &quot;Back&quot; key press event for WebView to go back to previous screen.
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && mainWebView.canGoBack()) {
            mainWebView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        if (mainWebView.isFocused() && mainWebView.canGoBack()) {
            mainWebView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CHROME_CUSTOM_TAB_REQUEST_CODE) {
            startActivity(new Intent(MainActivity.this, MainActivity.class));
        }

        if (requestCode == REQUEST_SELECT_FILE) {
            if (uploadMessage == null) return;
            uploadMessage.onReceiveValue(WebChromeClient.FileChooserParams.parseResult(resultCode, data));
            uploadMessage = null;
        }
    }

    @Override
    public void onServiceConnected(CustomTabsClient client) {
        mClient = client;
    }

    @Override
    public void onServiceDisconnected() {
        mClient = null;
    }

    public class MyWebChromeClient extends WebChromeClient {
        // reference to activity instance. May be unnecessary if your mainWebView chrome client is member class.
        MainActivity myActivity;

        @Override
        public boolean onCreateWindow(WebView view, boolean isDialog,
                                      boolean isUserGesture, Message resultMsg) {


            mWebviewPop = new WebView(MainActivity.this);
            mWebviewPop.getSettings().setDefaultTextEncodingName("utf-8");
            mWebviewPop.getSettings().setJavaScriptEnabled(true);
          //  mWebviewPop.getSettings().setUserAgentString("Mozilla/5.0 (Linux; Android 4.1.2; C1905 Build/15.1.C.2.8) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.112 Mobile Safari/537.36");
            mWebviewPop.getSettings().setUserAgentString(getString(R.string.app_name));

            // mWebviewPop.getSettings().setSupportZoom(true);
            // mWebviewPop.getSettings().setBuiltInZoomControls(true);
            mWebviewPop.getSettings().setSupportMultipleWindows(true);
            mWebviewPop.getSettings().setLoadWithOverviewMode(false);
            mWebviewPop.getSettings().setUseWideViewPort(true);
            //mWebviewPop.getSettings().setUserAgentString("Chrome/74.0.3729.112 Mobile");

            mWebviewPop.getSettings().setLoadsImagesAutomatically(true);
            mWebviewPop.getSettings().setAllowContentAccess(true);

            mWebviewPop.getSettings().setDomStorageEnabled(true);
            //mWebviewPop.clearView();
            mWebviewPop.setHorizontalScrollBarEnabled(false);
            mWebviewPop.getSettings().setAppCacheEnabled(true);
            mWebviewPop.getSettings().setDatabaseEnabled(true);
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                mWebviewPop.getSettings().setDatabasePath("/data/data/" + getPackageName() + "/databases/");
            }
            mWebviewPop.setVerticalScrollBarEnabled(false);
            //  mWebviewPop.getSettings().setDisplayZoomControls(true);
            mWebviewPop.getSettings().setAllowFileAccess(true);
            mWebviewPop.getSettings().setPluginState(WebSettings.PluginState.ON);
            mWebviewPop.setScrollbarFadingEnabled(false);
            mWebviewPop.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
            //  mWebviewPop.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
            mWebviewPop.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
            mWebviewPop.setInitialScale(1);

            mWebviewPop.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
            mWebviewPop.setWebViewClient(new myWebClient());
            mWebviewPop.setWebChromeClient(new WebChromeClient() {
                @Override
                public void onCloseWindow(WebView window) {
                    mContainer.removeView(mWebviewPop);
                }
            });

            mContainer.addView(mWebviewPop);
            WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
            transport.setWebView(mWebviewPop);
            resultMsg.sendToTarget();


            return true;
        }

        public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
            // make sure there is no existing message
            if (uploadMessage != null) {
                uploadMessage.onReceiveValue(null);
                uploadMessage = null;
            }

            uploadMessage = filePathCallback;

            Intent intent = fileChooserParams.createIntent();
            try {
                startActivityForResult(intent, MainActivity.REQUEST_SELECT_FILE);
            } catch (ActivityNotFoundException e) {
                uploadMessage = null;
                Toast.makeText(myActivity, "Cannot open file chooser", Toast.LENGTH_LONG).show();
                return false;
            }

            return true;
        }

        @Override
        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
            return super.onJsAlert(view, url, message, result);
        }

        @Override
        public void onCloseWindow(WebView window) {
            mContainer.removeView(mWebviewPop);
        }


    }

    public class myWebClient extends WebViewClient {




        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            String urlNew = " ";

            if (url.contains("google.com")) {
                urlNew = "https://accounts.google.com/o/oauth2/auth?redirect_uri=" +
                        "https://www.prulmarket.com/2PfkYA2Mg9/signin" +
                        "&client_id=751893613508-ipbjtdpv27ikj9b5ee2i70kgpu844eje.apps.googleusercontent.com"+
                        "&response_type=code" +
                        //"&scope=https://accounts.google.com/o/oauth2/auth" +
                        "&scope=openid email profile"+
                        "&ss_domain=https://prulmarket.com&fetch_basic_profile=true&gsiwebsdk=2";

                CustomTabsSession session = mClient.newSession(new NavigationCallback());

                Intent intent = new Intent(MainActivity.this, BrowserActionsReceiver.class);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(MainActivity.this, 0, intent, 0);


                CustomTabsIntent customTabsIntent = new CustomTabsIntent.Builder(session)
                        .setShowTitle(true)
                        .addMenuItem("Close", pendingIntent)
                        .setToolbarColor(ContextCompat.getColor(MainActivity.this, R.color.colorAccent))
                        .build();
                customTabsIntent.intent.setPackage(CHROME_PACKAGE_NAME);
                customTabsIntent.intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                CustomTabsHelper.addKeepAliveExtra(MainActivity.this, customTabsIntent.intent);
                //startActivityForResult(customTabsIntent.intent, CHROME_CUSTOM_TAB_REQUEST_CODE);
                customTabsIntent.launchUrl(MainActivity.this, Uri.parse(urlNew));
                // This is where the magic happens...
                // CustomTabActivityHelper.openCustomTab(MainActivity.this, customTabsIntent,Uri.parse(urlNew),new WebviewFallback());

                CustomTabsClient.bindCustomTabsService(MainActivity.this, "com.android.chrome", mConnection);//mention package name which can handle the CCT their many browser present.

                //view.loadUrl(urlNew);
                return false;
            }else {
                urlNew = url;
                view.loadUrl(urlNew);
                return true;
            }

            /* else if(url.contains("facebook")){
                mainWebView.loadUrl("javascript:testEcho('Hello JavaScript!')");
                return true;
            } */

        }


        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            Log.e("log", "Error: " + description);
            Log.e("MainActivity"," Error occured while loading the web page at Url"+ failingUrl+"." +description);
            view.loadUrl("about:blank");
            Toast.makeText(MainActivity.this, "Error occured, please check newtwork connectivity", Toast.LENGTH_SHORT).show();
            super.onReceivedError(view, errorCode, description, failingUrl);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();

        //Toast.makeText(this, "OnResume Event Called", Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        String action = intent.getAction();
        String data = intent.getDataString();

        if(null != intent.getExtras()){
            for (String key : intent.getExtras().keySet()) {
                Object value = intent.getExtras().get(key);
                Log.d("MainActivity: ", "Key: " + key + " Value: " + value);

                notificationUrl = value.toString();
            }
            if(null == notificationUrl) {
                mainWebView.loadUrl("https://prulmarket.com/2PfkYA2Mg9/home");
            }else{
                mainWebView.loadUrl(notificationUrl);
            }
        }

    }

    private class NavigationCallback extends CustomTabsCallback {
        @Override
        public void onNavigationEvent(int navigationEvent, Bundle extras) {
            Log.w("MainActivity", "onNavigationEvent: Code = " + navigationEvent);
            switch (navigationEvent) {
                case NAVIGATION_STARTED:
                    // Sent when the tab has started loading a page.
                    break;
                case NAVIGATION_FINISHED:
                    // Sent when the tab has finished loading a page.
                    break;
                case NAVIGATION_FAILED:
                    // Sent when the tab couldn't finish loading due to a failure.
                    break;
                case NAVIGATION_ABORTED:
                    // Sent when loading was aborted by a user action before it finishes like clicking on a link
                    // or refreshing the page.
                    break;
                case TAB_SHOWN:

                    break;
            }
        }

    }

    private void bindCustomTabsService() {
        if (mClient != null) return;
        if (TextUtils.isEmpty(mPackageNameToBind)) {
            mPackageNameToBind = CustomTabsHelper.getPackageNameToUse(this);
            if (mPackageNameToBind == null) return;
        }
        mConnection = new ServiceConnection(MainActivity.this);
        boolean ok = CustomTabsClient.bindCustomTabsService(this, mPackageNameToBind, mConnection);
        if (ok) {
            //mConnectButton.setEnabled(false);
        } else {
            mConnection = null;
        }
    }


}
