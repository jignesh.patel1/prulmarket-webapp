package com.iwant.marketplace.business.prulmarket;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.RemoteMessage;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

/**
 * Created by jignesh on 23/11/17.
 */

public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {

    private Context context;

    public FirebaseMessagingService() {
        this.context = FirebaseMessagingService.this;
    }

    public void onMessageReceived(RemoteMessage remoteMessage) {
        // ...

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d("log", "Message: " + remoteMessage);

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d("log", "Message data payload: " + remoteMessage.getData());
            getNotificationData(remoteMessage.getData(), "");
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d("log", "Message Notification Body: " + remoteMessage.getNotification());
            generateNotification(null, remoteMessage.getNotification().getTitle());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    private void getNotificationData(Map<String, String> notificationData, String title) {
        generateNotification(notificationData, title);
        //saveNotification(notificationData);
    }

    private void generateNotification(Map<String, String> notificationData, String titleNew) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        //  String title=notificationData.get("title");
        //  String url = notificationData.get("url");
        //  String description = notificationData.get("description");

        String url = "https://jablickar.cz/wp-content/uploads/2013/08/iwant-icon-840x440.jpg";
        String description = "Test Image";

        int icon = R.mipmap.ic_launcher;
        Bitmap bitmap = null;
        if (url != null && url.toString().length() > 0) {
            bitmap = getImageBitmap(url);
        }
        long when = System.currentTimeMillis();

       /* String orderId = "";
        JSONObject jsonObject = null;
        try {
            if(notificationData.containsKey("body")) {
                jsonObject = new JSONObject(notificationData.get("body"));
                if(jsonObject.has("orderId")) {
                    orderId = jsonObject.optString("orderId");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

        Intent notificationIntent = new Intent(context, MainActivity.class);
        notificationIntent.putExtra("URL", "https://www.prulmarket.com/2PfkYA2Mg9/order-details/SO129031900333");
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setContentTitle(!titleNew.isEmpty() ? titleNew : notificationData.get("title"))
                .setContentText(description)
                .setContentIntent(pendingIntent)
                .setSmallIcon(icon)
                .setAutoCancel(true)
                .setWhen(when);

        if (bitmap != null) {
            notificationBuilder.setStyle(new NotificationCompat.BigPictureStyle()
                    .bigPicture(bitmap));
            notificationBuilder.setSubText(description);
        }

        Notification notification = notificationBuilder.build();
        notificationManager.notify((int) when, notification);
    }

   /* private void saveNotification(Map<String, String> notificationData) {
        NotificationRepository notificationRepository = new NotificationRepository();
        NotificationRes notificationRes = new NotificationRes();

        String title=notificationData.get("title");
        String url = notificationData.get("url");
        String description = notificationData.get("description");

        String orderId = "";
        String orderNumber = "";
        String orderDate = "";
        String notificationType = "";
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(notificationData.get("body"));
            orderId = jsonObject.getString("orderId");
            orderNumber = jsonObject.getString("orderNumber");
            orderDate = jsonObject.getString("dateTime");
            notificationType = jsonObject.getString("notificationType");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        notificationRes.setTitle(title);
        notificationRes.setUrl(url);
        notificationRes.setId(String.valueOf(System.currentTimeMillis()));
        notificationRes.setCreatedAt(String.valueOf(System.currentTimeMillis()));
        notificationRes.setDescription(description);
        notificationRes.setOrderId(orderId);
        notificationRes.setNotificationType(notificationType);
        notificationRes.setOrderDate(orderDate);
        notificationRes.setOrderNumber(orderNumber);

        notificationRepository.addNotificationAsync(notificationRes);
    }*/

    private Bitmap getImageBitmap(String imageUrl) {

        Bitmap myBitmap = null;
        InputStream in;
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            in = connection.getInputStream();
            myBitmap = BitmapFactory.decodeStream(in);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return myBitmap;
    }

}
